@extends('layout.public')

@section('content')
@if (session('status'))
<div>
    {{ session('status') }}
</div>
@endif

@if ($errors->any())
<div>
    <div>{{ __('Whoops! Something went wrong.') }}</div>

    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

<form class="user" method="POST" action="{{ route('login') }}">
    @csrf

    <div class="form-group">
        <input type="email" class="form-control form-control-user" name="email" value="{{ old('email') }}" required
            autofocus aria-describedby="emailHelp" placeholder="Enter Email Address..." />
    </div>

    <div class="form-group">
        <input type="password" class="form-control form-control-user" name="password" required
            autocomplete="current-password" placeholder="Password" />
    </div>

    <!--<div>
            <label>{{ __('Remember me') }}</label>
            <input type="checkbox" name="remember">
        </div>-->
    <div class="form-group">
        <div class="custom-control custom-checkbox small">
            <input type="checkbox" class="custom-control-input" name="remember" id="customCheck">
            <label class="custom-control-label" for="customCheck">Remember
                Me</label>
        </div>

        <div>
            <button class="btn btn-primary btn-user btn-block" type="submit">
                {{ __('Login') }}
            </button>
        </div>

        <hr>
        @if (Route::has('password.request'))
        <div class="text-center">
            <a class="small" href="{{ route('password.request') }}">
                Forgot Password?
            </a>
            @endif
        </div>

        <div class="text-center">
            <a class="small" href="/register">Create an Account!</a>
        </div>





</form>
@endsection