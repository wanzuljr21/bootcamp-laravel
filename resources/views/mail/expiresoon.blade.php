<!DOCTYPE html>
<html lang="en">

<head>
    <meta name="viewport" content="width=device-width,
initial-scale=1.0">
    <title>Selamat Datang</title>
    <style>
    body {
        background-color: #ddd;
    }

    table {
        width: 90%;
        margin: auto;
        max-width: 800px;
        background-color: #fff;
        border: 1px solid #aaa;
        border-radius: 10px;
        font-family: arial;
        padding: 20px;
    }

    h1 {
        margin-top: 0px;
    }
    </style>
</head>

<body>
    <table cellpadding="0" cellspacing="0" border="0">
        <tr>
            <td>
                <h1>Akaun anda bakal luput</h1>

                {{$name}}
                <p>akaun anda di bootcamp bakal luput tarikh. Sila perbaharui keahlian dengan mebuat pembayaran di halaman berikut.</p>

                <p>Tarikh luput Keahlian :{{$expire_at}}</p>
                <p><a href="{{route('signup')}}"> Bayar yuran bootcamp</a></p>

                <p>sistem keahlian bootcamp</p>
            </td>
        </tr>
    </table>
</body>

</html>