<?php

use App\Mail\ExpireSoonEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PlanController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\SignUpController;
use App\Http\Controllers\PaymentController;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {return view('main');});

Route::get('/member/login', [ MemberController::class, 'login' ] );

Route::get('/member/register', [ MemberController::class, 'register' ]);

Route::get('/member', [ MemberController::class, 'index' ]);

Route::get('/member/profile', [ MemberController::class, 'profile' ]);

Route::get('/dashboard', [MemberController::class, 'index']) ->name('dashboard') ->middleware(['auth']);

Route::middleware(['auth','can:is-admin'])->group(function (){

// --Admin users module
Route::resource('admin/user' , UserController::class);

// --Roles user module
Route::resource('admin/role' , RoleController::class);

// --Plan module
Route::resource('admin/plan', PlanController::class);

// --Payment module
Route::get('admin/payment', [PaymentController::class, 'index'] );


});

Route::resource('signup', SignUpController::class);
Route::get('/signup', [SignUpController::class, 'index'])->name('signup');

Route::get('/signup/review/{id}', [SignUpController::class, 'review']);
Route::post('/signup/go/{payment_gateway}', [SignUpController::class, 'go'])->name('signup.go');

/** PAYMENT GATEWAY REDIRECT */
Route::post('/signup/thankyou/{payment_gateway}', [SignUpController::class, 'thankyou'])->name('signup.thankyou');
Route::get('/signup/thankyou/{payment_gateway}', [SignUpController::class, 'thankyou']);

/** PAYMENT GATEWAY CALLBACK--TAK LEH CHECK/UJI DALAM DEVELOPMENT */
Route::post('/signup/callback/{payment_gateway}', [SignUpController::class, 'payment_callback'])->name('signup.callback');
Route::get('/signup/callback/{payment_gateway}', [SignUpController::class, 'payment_callback']);

Route::view('/rahsia', 'rahsia')->middleware(['auth','can:is-active']);

/**EMAIL VIEW */
// Route::get('/email/expiresoon', function () {
//     return new App\Mail\ExpireSoonEmail();
//     });

Route::get('/email/test',function(){

    $data = [
        'name' => 'wan zul',
        'expire_at' => '2021-01-21'
    ];
    Mail::to('ceasar31@example.net')->send(new ExpireSoonEmail($data));

    echo"email sent";
});